---
layout: handbook-page-toc
title: "Secure and Defend UX"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Overview
We’re designing an experience that enables contributors to commit their most secure work and to defend what they have in production. This is done by merging security into the DevOps process, giving development teams more ownership, commonly referred to as DevSecOps. The experience brings cross-functional stakeholders together to make better, faster, and more security-oriented decisions. We are doing this by focusing the experience on automation, education, empowerment, and shifting security to the left.

**Automation** relates to convention over configuration that helps draw a clear path for the user to produce meaningful results. When it comes to web security, no application will ever be 100% secure. That’s why we are focused on integrating automation into every step of the user’s journey, taking the guesswork out of configuration to open up more time on what’s important: resolving known vulnerabilities and identifying attacks or threats.

**Education** for our users so they understand security basics and are aware of security needs in their applications. We want our users to know where vulnerabilities or threats have been detected, visualize the implications, present resources to understand the problem, and provide the tools to facilitate informed decisions about next steps.

**Empowerment** for all users to resolve security issues is essential as cross-functional departments share ownership of security. Our tools strive for an experience where the developer is responsible and the security team is accountable for the organization's security.

**Shifting left** is taking things like QA and other processes typically found later in the ops cycle and moving them to development. Resulting in security problems being addressed early and more often.

### Customer
Organizations of all sizes benefit from our tools and the experience of bringing teams together. We provide customers value with workflow efficiency, informed team decision-making, lower risk of security breaches, and attaining compliance requirements. We focus on all aspects of the product — starting with the customer experience. When deciding to use our tools, organizations are often considering the following:

* What languages does the tool support?
* What tests do we need to cover?
* What tests does the tool cover?
* Can it be automated?
* How long will setup take?
* What does setup involve?
* How easy is it to use?
* What technologies do you need to use? (ex. Docker, Kubernetes)
* How lightweight is the tool?
* How does it integrate with our tools?
* How does it integrate with GitLab's product?
* What customer support is offered?
* What are the upcoming features? (we are selling contracted services vs monthly)

### Team
* [Valerie Karnes](https://gitlab.com/vkarnes) - UX Manager, Secure & Defend
* [Tali Lavi](https://gitlab.com/tlavi) - UX Researcher, Secure & Defend
* [Kyle Mann](https://gitlab.com/kmann) - Sr. Product Designer, Software Composition Analysis, Compliance & Auditing (Secure)
* [Camellia Yang](https://gitlab.com/cam.x) - Sr. Product Designer, Static and Dynamic Testing, Security Testing (Secure)
* [Annabel Dunstone Gray](https://gitlab.com/annabeldunstone) - Product Designer, Static and Dynamic Testing, Code Scanning (Secure)
* [Andy Volpe](https://gitlab.com/andyvolpe) - Sr. Product Designer (Defend)
* [Becka Lippert](https://gitlab.com/beckalippert) - Product Designer (Defend)
* [Hiring!](https://about.gitlab.com/jobs/apply/product-designer-secure-4331998002/) - Product Designer, Secure
* Position Opening Soon - Sr. Product Designer, Secure 
* Position Opening Soon - Sr. Product Designer, Defend

### How we work
See our [UX Workflow](https://about.gitlab.com/handbook/engineering/ux/ux-department-workflow/#how-we-use-labels) page for more on how our teams use labels.

While there is a lot of overlap between our two teams, there are some things that are specific to each group. Read more about our individual teams on the [Secure UX](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/secure/) page and the [Defend UX](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/defend/) pages.

### Our strategy
The Secure and Defend UX teams are working together to [uncover customers core needs](https://gitlab.com/groups/gitlab-org/-/epics/1611), what our users’ workflows looks like, and defining how we can make our users tasks easier. Our strategy involves the following actions:

* [UX Scorecards and recommendations](/handbook/engineering/ux/ux-scorecards/) (quarterly)
* Internal understanding: stakeholder interviews (annually)
* Iterating on Secure product [foundation's document](https://gitlab.com/gitlab-org/gitlab-design/issues/333) (ongoing)
* Iterating on Defend product [foundation's document](https://gitlab.com/gitlab-org/gitlab-design/issues/547) (ongoing)
* Performing heuristic evaluations on at least 3 competitors, based competitors the 3 user type is using (annually, ongoing)
* We talk to our customer (ongoing)
* We talk to our users (ongoing)
* We outline current user workflows and improve them (upcoming, ongoing)

Additionally, we value the following:
* Testing our features with usefulness and usability studies
* Drinking our own wine and partnering closely with our internal Security and Compliance teams for feedback and feature adoption
* Partnering with our sales and account team to connect directly with customers and learn why customers did (or didn’t) choose our product
* Collaborating with stakeholders on proof of concept discoveries to better understand future consideration
* Collaborating between the Secure and Defend teams to make sure we’re offering a consistent and cohesive security workflow so that our customers can apply reactive findings into proactive measures and make sure their applications are protected throughout the entire application lifecycle 
* Prioritizing issues that are likely to increase our number of active users

The source of truth lives with shipped features, therefore we:
* Make data-driven decisions quickly and thoughtfully
* Optimize to deliver our solutions as soon as possible
* Learn, iterate, test, and repeat

### Follow our work
Our [Secure and Defend UX YouTube channel](https://www.youtube.com/playlist?list=PL05JrBw4t0KrFCe5BgUkzFrZifjforQOz) includes UX Scorecard walkthroughs, UX reviews, group feedback sessions, team meetings, and more.


